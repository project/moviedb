<div class="moviedb-person">
<?php if(count($filmography)) { ?>
<div class="filmography">
<?php
  foreach($filmography as $list) {
    print $list;
  }
?>
</div>
<?php } ?>
<?php if($node->body) { ?>
<div class="biography">
<h2>Biography</h2>
<div class="content"><?php print $node->body; ?></div>
</div>
<?php } ?>
</div>
