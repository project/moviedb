
About
-----
The moviedb module allows you to manage a large movie database. moviedb creates separate nodes for movie, actors and companies.

Install
-------
1- Upload the moviedb folder to the drupal modules folder.
2- Activate the moviedb module in drupal.

Setup Genres
------------
To get more flexibility, moviedb uses the taxonomy module to assign genres to movies. Follow these steps to get genres tagging working

1- Make sure the taxonomy module is activated
2- Create a freetagging vocabulary that will contain movie genres and assign it to the Movie node type
3- Go the the moviedb setup page and select the vocabulary you want to use for genres
4- You're done. Each movie page can now be tagged with genres.

Problems
--------

There are a couple of bugs in this script. Not least the inability to create offsite links. However, because of CCK's much better way of doing this, I have decided to devote my time to creating a 6.x version of this module, rather than sort out this.

Any bugs you find, please report at http://www.drupal.org/project/moviedb