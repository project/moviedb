<?php

/**
 * @file
 * Control the Movie page output
 */


/**
 * Implementation of hook_access().
 *
 */
function mdb_movie_access($op, $node) {

  global $user;

  if ($op == 'view' && user_access('view movies')) {
  return $node->status;
  }

  if ($op == 'create') {
  return user_access('create movies');
  }

  if ($op == 'update' || $op == 'delete') {
  if (user_access('edit own movies') && ($user->uid == $node->uid)) {
  return TRUE;
  }
  }
}

/**
 * Implementation of hook_insert().
 */
function mdb_movie_insert($node) {

  //Insert the data in the moviedb table
  db_query("INSERT INTO {moviedb} (nid, movietitle, releaseyear,  cover, runtime, country, language, imdb, amazon ) VALUES (%d, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", $node->nid, $node->title,  $node->releaseyear, $node->cover, $node->runtime,  $node->country, $node->language,  $node->imdb, $node->amazon);

  //Add the actors
  moviedb_people_process($node->nid, $node->actors_form, 'actors');

  //Add the directors
  moviedb_people_process($node->nid, $node->directors_form, 'directors');

  //Add the writers
  moviedb_people_process($node->nid, $node->writers_form, 'writers');

  //Add the producers
  moviedb_people_process($node->nid, $node->producers_form, 'producers');


  for ($i = 0; $i < $node->numlinks; $i++) {
    $movielink = "movielink-$i";
    $linkdescription = "linkdescription-$i";
    if ($node->$movielink) {
    db_query("INSERT INTO {moviedb_links} (nid, movielink, description, weight) VALUES (%d, '%s', '%s', %d)", $node->nid, $node->$movielink, $node->$linkdescription, $i);
    }
  }

}

/**
 * Implementation of hook_load().
 *
 */
function mdb_movie_load($node) {
  $moviedb = db_fetch_object(db_query("SELECT '%s' {moviedb} WHERE nid = '$node->nid'"));

  $actors                   = moviedb_people_load($node->nid, 'actors');
  $moviedb->actors          = $actors['object'];
  $moviedb->actors_form     = $actors['form'];

  $directors                = moviedb_people_load($node->nid, 'directors');
  $moviedb->directors       = $directors['object'];
  $moviedb->directors_form  = $directors['form'];

  $writers                  = moviedb_people_load($node->nid, 'writers');
  $moviedb->writers         = $writers['object'];
  $moviedb->writers_form    = $writers['form'];

  $producers                = moviedb_people_load($node->nid, 'producers');
  $moviedb->producers       = $producers['object'];
  $moviedb->producers_form  = $producers['form'];

  if (module_exists('taxonomy') && ($vid = variable_get('moviedb_genre_vocab', 0)) != 0) {
  $terms = taxonomy_node_get_terms_by_vocabulary($node->nid, $vid);
  }
  if ($terms) {
    $moviedb->genre         = $terms;
  }



  $movielinks = db_query("SELECT movielink, description, weight FROM {moviedb_links} WHERE nid = '%s' ORDER BY weight", '$node->nid');
  while ($movielink = db_fetch_object($movielinks)) {
    $moviedb->movielinks[$movielink->weight] = $movielink->movielink;
    $moviedb->linkdescriptions[$movielink->weight] = $movielink->description;
  }
  $moviedb->numlinks = count($moviedb->movielinks);

  return $moviedb;
}

/**
 * Implementation of hook_update().
 *
 */
function mdb_movie_update($node) {

  //Update the moviedb table
  db_query("UPDATE {moviedb} SET movietitle = '%s', releaseyear = '%s', cover = '%s', runtime = '%s', country = '%s', language = '%s', imdb = '%s', amazon = '%s' WHERE nid = %d", $node->title,  $node->releaseyear,  $node->cover, $node->runtime,  $node->country, $node->language,  $node->imdb, $node->amazon, $node->nid);


  //Delete all the records from the actor links table
  db_query("DELETE FROM {moviedb_actors} WHERE mid = %d", $node->nid);
  //Add the actors
  moviedb_people_process($node->nid, $node->actors_form, 'actors');


  //Delete all the records from the directors links table
  db_query("DELETE FROM {moviedb_directors} WHERE mid = %d", $node->nid);
  //Add the directors
  moviedb_people_process($node->nid, $node->directors_form, 'directors');

  //Delete all the records from the writers links table
  db_query("DELETE FROM {moviedb_writers} WHERE mid = %d", $node->nid);
  //Add the writers
  moviedb_people_process($node->nid, $node->writers_form, 'writers');

  //Delete all the records from the producers links table
  db_query("DELETE FROM {moviedb_producers} WHERE mid = %d", $node->nid);
  //Add the producers
  moviedb_people_process($node->nid, $node->producers_form, 'producers');


  db_query("DELETE FROM {moviedb_links} WHERE nid = %d", $node->nid);
  for ($i = 0; $i < $node->numlinks; $i++) {
    $movielink = "movielink-$i";
    $linkdescription = "linkdescription-$i";
    if ($node->$movielink) {
      db_query("INSERT INTO {moviedb_links} (nid, movielink, description, weight) VALUES (%d, '%s', '%s', %d)", $node->nid, $node->$movielink, $node->$linkdescription, $i);
    }
  }

}

/**
 * Implementation of hook_delete().
 *
 */
function mdb_movie_delete($node) {
  db_query("DELETE FROM {moviedb} WHERE nid = %d", $node->nid);
  db_query("DELETE FROM {moviedb_actors} WHERE mid = %d", $node->nid);
  db_query("DELETE FROM {moviedb_directors} WHERE mid = %d", $node->nid);
  db_query("DELETE FROM {moviedb_writers} WHERE mid = %d", $node->nid);
  db_query("DELETE FROM {moviedb_producers} WHERE mid = %d", $node->nid);
  db_query("DELETE FROM {moviedb_links} WHERE nid = %d", $node->nid);
}

/**
 * Implementation of hook_submit().
 *
 */
function mdb_movie_submit(&$node) {
}

/**
 * Implementation of validate().
 *
 */
function mdb_movie_validate(&$node) {
}

/**
 * Implementation of hook_form().
 *
 */
function mdb_movie_form(&$node) {

if ($node->movielinks) {
    $node->numlinks = count($node->movielinks);
  }
  else {
    $node->numlinks = 3;
  }

drupal_add_js('
var offlinks = '. $node->numlinks .';
function mdb_add_offsite() {
  numlinks = offlinks;
  speed = "slow";
  var linkcode;

  linkcode = "<tr><td><div class=\"form-item\"><label for=\"edit-movielink-"+numlinks+"\">URL: </label><input type=\"text\" maxlength=\"255\" name=\"movielink-"+numlinks+"\" id=\"edit-movielink-"+numlinks+"\"  size=\"45\" class=\"form-text\" /></div></td><td><div class=\"form-item\"><label for=\"edit-linkdescription-"+numlinks+"\">Description: </label><input type=\"text\" maxlength=\"255\" name=\"linkdescription-"+numlinks+"\" id=\"edit-linkdescription-"+numlinks+"\"  size=\"45\" class=\"form-text\" /></div></td></tr>";

 $("#offsite-links").append(linkcode);
  offlinks++;
}
', 'inline');

  $form['numlinks'] = array(
    '#type' => 'hidden',
    '#value' => $node->numlinks,
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $node->title,
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('The title of the movie being reviewed.'),
  );


  $form['general'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('General Info'),
    '#collapsible'  => TRUE,
    '#weight'       => 0,

  );


  $form['general']['releaseyear'] = array(
    '#type' => 'textfield',
    '#title' => t('Release year'),
    '#default_value' => $node->releaseyear,
    '#size' => 4,
    '#maxlength' => 255,
    '#description' => t('The year the movie was released.  (Format:  YYYY).'),
  );





  $form['general']['cover'] = array(
    '#type' => 'textfield',
    '#title' => t('Cover picture'),
    '#default_value' => $node->cover,
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('URL of movie cover image.'),
  );


    $form['general']['runtime'] = array(
    '#type' => 'textfield',
    '#title' => t('Runtime'),
    '#default_value' => $node->runtime,
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('The movie run time.'),
  );

  $form['general']['country'] = array(
    '#type' => 'textfield',
    '#title' => t('Country'),
    '#default_value' => $node->country,
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Country of movie production.'),
  );

  $form['general']['language'] = array(
    '#type' => 'textfield',
    '#title' => t('Language'),
    '#default_value' => $node->language,
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Language of the movie.'),
  );

  $form['general']['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Synopsis'),
    '#default_value' => $node->body,
    '#cols' => 50,
    '#rows' => 6,
    '#description' => t('A synopsis of the movie being reviewed.'),
  );

  $form['people'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('People'),
    '#collapsible'  => TRUE,
    '#weight'       => 2,

  );

  $form['people']['actors_form'] = array(
    '#type'           => 'textarea',
    '#title'          => t('Actors'),
    '#default_value'  => $node->actors_form,
    '#description'    => t('Enter the name of the actors. Comma separated. eg: Mel Gibson, Tom Cruise'),
    '#cols'           => 50,
    '#rows'           =>  4,
    '#weight'         =>  0,
  );

  $form['people']['directors_form'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Director'),
    '#default_value'  => $node->directors_form,
    '#size'           => 60,
    '#description'    => t('Director(s) of the movie. Comma separated.'),
    '#weight'         => 4,
    '#autocomplete_path'  => 'moviedb/person/autocomplete',
  );

  $form['people']['writers_form'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Writers'),
    '#default_value'  => $node->writers_form,
    '#size'           => 60,
    '#description'    => t('Writers of the movie. Comma separated.'),
    '#weight'         => 6,
    '#autocomplete_path'  => 'moviedb/person/autocomplete',
  );

  $form['people']['producers_form'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Producers'),
    '#default_value'  => $node->producers_form,
    '#size'           => 60,
    '#description'    => t('Producers of the movie. Comma separated.'),
    '#weight'         => 8,
    '#autocomplete_path'  => 'moviedb/person/autocomplete',
  );



  $form['links'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('Links'),
    '#collapsible'  => TRUE,
    '#weight'       => 5,

  );

  $form['links']['imdb'] = array(
    '#type' => 'textfield',
    '#title' => t('Imdb'),
    '#default_value' => $node->imdb,
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Imdb id of the movie'),
  );

  $form['links']['amazon'] = array(
    '#type' => 'textfield',
    '#title' => t('Amazon'),
    '#default_value' => $node->amazon,
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Amazon ASIN of the movie'),
  );


  $form['links']['offsite_links'] = array(
    '#type' => 'fieldset',
    '#title' => t('Offsite links'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['links']['offsite_links']['movielinks_begin'] = array(
      '#type' => 'markup',
      '#value' => '<table id="offsite-links">',
  );
  for ($i = 0; $i < $node->numlinks; $i++) {
    $form['links']['offsite_links']["movielink-$i"] = array(
      '#type' => 'textfield',
      '#title' => 'URL',
      '#default_value' => $node->movielinks[$i],
      '#size' => 45,
      '#maxlength' => 255,
      '#description' => '',
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    );
    $form['links']['offsite_links']["linkdescription-$i"] = array(
      '#type' => 'textfield',
      '#title' => 'Description',
      '#default_value' => $node->linkdescriptions[$i],
      '#size' => 45,
      '#maxlength' => 255,
      '#description' => '',
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    );
  }
  $form['links']['offsite_links']['movielinks_end'] = array(
      '#type' => 'markup',
      '#value' => '</table>',
  );

  $form['links']['offsite_links']['mdb_movie_more_links'] = array(
    '#type' => 'button',
    '#button_type' => 'offsite_but',
    '#name' => 'add_offsite',
    '#submit' => FALSE,
    '#value' => t('add more links'),
    '#attributes' => array('onclick' => 'mdb_add_offsite();return false;'),
  );

  $form['format'] = filter_form($node->format, 8);

  return $form;

}

/**
 * Implementation of hook_view().
 *
 */
function mdb_movie_view(&$node, $teaser = FALSE, $page = FALSE) {
  $node = node_prepare($node, $teaser);

  $node->content['data'] = array(
  '#value' => theme('mdb_movie', $node),
  '#weight' => 0, );
  $node->content['body']['#value'] = '';

  return $node;
}