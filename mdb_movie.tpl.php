<div class="moviedb">
<div class="movie_info">
<?php if($node->releaseyear) { print $node->releaseyear.". \n"; } ?>
<?php if($node->country) {     print $node->country .". \n"; } ?>
<?php if($node->language) {    print $node->language .". \n"; } ?>
 <?php if($node->runtime) {    print $node->runtime ." minutes. \n"; } ?>
</div> 
<div class="clear-block">
<?php if($node->cover) { ?>
<div class="cover">
<?php print theme("image", $node->cover, "Cover for ".$node->title." (".$node->releaseyear.")",  "Cover for ".$node->title." (".$node->releaseyear.")"); ?>
</div>
<?php } ?>
<div class="castcrew">
<?php
foreach($castcrew as $list) {
  print $list; 
} ?>
</div>
<?php /* if($node->imdb) { 
print "<div class=\"label3\">". t('IMDb') .": </div><div class=\"content3\">". "<a href=\"http://www.imdb.com/title/". $node->imdb ."/\" target=\"_blank\">" . $node->imdb . "</a></div>\n";
} */ ?>                                                 
</div>
<div class="clear-block">
<?php if($node->body) { ?>
<div class="label1">Review</div>
<div class="content">
<?php if($submitted) { print $submitted; } ?>
<?php print $node->body; ?>
<?php } ?>
</div>
</div>
</div>
