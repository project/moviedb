<?php

/**
 * @file
 * Control the theme output
 */
function theme_mdb_movie(&$node) {

  $output = "<div class=\"moviedb\">\n";
  $output .= "<table width=\"100%\">";
  $output .= "<tr><td colspan=\"2\">&nbsp;</td></tr>";
  $output .= "<tr><td width=\"15%\" class=\"mr_cover\" align=\"left\" valign=\"top\">";
  if ($node->cover) {
    $output .= "    <div class=\"cover\"><img src=\"$node->cover\"></div>\n";
  }
  $output .= "</td>";
  $output .= "<td width=\"85%\" class=\"mr_detail\">";
  if ($node->actors[0]) {
    $actors = theme_mdb_movie_people($node->actors);
    $output .= "    <div class=\"label2\">". t("Starring") .": </div><div class=\"content2\">". $actors ."</div>";
  }

  if ($node->directors[0]) {
    $directors = theme_mdb_movie_people($node->directors);
    $output .= "    <div class=\"label2\">". t('Directed by') .": </div><div class=\"content2\">". $directors ."</div>\n";
  }
  if ($node->writers[0]) {
    $writers = theme_mdb_movie_people($node->writers);
    $output .= "    <div class=\"label2\">". t('Written by') .": </div><div class=\"content2\">". $writers ."</div>\n";
  }

  if ($node->producers[0]) {
    $producers = theme_mdb_movie_people($node->producers);
    $output .= "    <div class=\"label2\">". t('Produced by') .": </div><div class=\"content2\">". $producers ."</div>\n";
  }

  if ($node->genre) {
    foreach ($node->genre as $term) {
      if ($tlist != '') {
  $tlist .= ', ';
      }
      $tlist .= l($term->name, 'taxonomy/term/'. $term->tid);
    }
    if ($tlist) {
     $output .= "<div class=\"label2\">". t('Genre') .": </div><div class=\"content2\">". $tlist ."</div>\n";
    }
  }


  if ($node->releaseyear) {
    $output .= "    <div class=\"label2\">". t('Year') .": </div><div class=\"content2\">". $node->releaseyear ."</div>\n";
  }
  if ($node->country) {
    $output .= "    <div class=\"label2\">". t('Country') .": </div><div class=\"content2\">". $node->country ."</div>\n";
  }
  if ($node->language) {
    $output .= "    <div class=\"label2\">". t('Language') .": </div><div class=\"content2\">". $node->language ."</div>\n";
  }
  if ($node->runtime) {
    $output .= "    <div class=\"label2\">". t('Runtime') .": </div><div class=\"content2\">". $node->runtime ."</div>\n";
  }
  if ($node->imdb) {
    $output .= "    <div class=\"label2\">". t('Imdb') .": </div><div class=\"content2\">"."<a href=\"http://www.imdb.com/title/". $node->imdb ."/\" target=\"_blank\">". $node->imdb ."</a></div>\n";
  }


  $output .= "</td>";
  $output .= "</tr>";
  $output .= "<tr><td colspan=\"2\">&nbsp;</td></tr>";
  $output .= "</table>";


  $output .= " \n"; // end of header
  if ($node->body) {
    $output .= "  <div class=\"synopsis\">\n";
    $output .= "    <span class=\"label4\">". t('Synopsis') ."</span><br />\n";
    $output .= "    <span class=\"content2\">". $node->body ."</span>\n";
    $output .= "  </div><br />\n";
  }
  if ($node->movielinks[0]) {
    $output .= "  <div class=\"movielinks\">\n";
    $output .= "    <span class=\"label4\">". t('Related links') .":</span><br />\n";
    $numlinks = count($node->movielinks);
    for ($i = 0; $i < $numlinks; $i++) {
      $movielink = $node->movielinks[$i];
      $linkdescription = $node->linkdescriptions[$i] ? $node->linkdescriptions[$i] : $node->movielinks[$i];
      if (ereg('^http://|^https://|ftp://', "$movielink")) {
        // off site link
        $movielink = "<a href=\"$movielink\">". check_markup($linkdescription, $node->format) .'</a>';
      }
      elseif ($movielink) {
        // on site link
        $movielink = l("$linkdescription", "$movielink");
      }
      $output .= "    <span class=\"content2\">". check_markup($movielink, $node->format) ."</span>\n";
    }
    $output .= "  </div>\n";
  }
  $output .= "</div>"; // end of moviedb

  return $output;
}

/**
 * Theme function to format people links
 *
 */
function theme_mdb_movie_people($people) {

  foreach ($people as $person) {
      if ($return)
        $return .= ', '. l($person->title, 'node/'. $person->nid);
      else
        $return = l($person->title, 'node/'. $person->nid);
    }

    return $return;
}


/**
 * Formats the person node to show a bigraphy and a filmography
 *
 * @param node $node
 * The node
 * @return
 * String of The formatted body
 */
function theme_mdb_person($node) {

  $roles = array();

  if (count($node->actor)) {
    $items = array();
    foreach ($node->actor as $movie) {
      $items[] = l($movie->title .' ('. $movie->releaseyear .')', 'node/'. $movie->nid);

    }
    $roles[] = theme_item_list($items, t('Actor'), 'ol');

  }

  if (count($node->director)) {
    $items = array();
    foreach ($node->director as $movie) {
      $items[] = l($movie->title .' ('. $movie->releaseyear .')', 'node/'. $movie->nid);

    }
    $roles[] = theme_item_list($items, t('Director'), 'ol');

  }

  if (count($node->writer)) {
    $items = array();
    foreach ($node->writer as $movie) {
      $items[] = l($movie->title .' ('. $movie->releaseyear .')', 'node/'. $movie->nid);

    }
    $roles[] = theme_item_list($items, t('Writer'), 'ol');

  }

  if (count($node->producer)) {
    $items = array();
    foreach ($node->producer as $movie) {
      $items[] = l($movie->title .' ('. $movie->releaseyear .')', 'node/'. $movie->nid);

    }
    $roles[] = theme_item_list($items, t('Producer'), 'ol');

  }

  usort($roles, 'moviedb_person_usort');

  if (!empty($node->body)) {

  $body = '<h2>'. t('Biography') .'</h2>';
  $body .= check_markup($node->body, $node->format);
  }
  else {
  $body = '';
  }

  $body .= '<h2>'. t('Filmography') .'</h2>';

  foreach ($roles as $role) {
  $body .= $role;

  }

  return $body;
}