<?php

/**
 * @file
 * The module file - sets up the MovieDB module.
 */


//Some code adapted from moviereview module written by Emiliano <emiliano@webinteligente.com.br>
//


/**
 * Include functions for the movie node
 *
 */
include_once('mdb_movie.inc');
/**
 * Include functions for the person node
 *
 */
include_once('mdb_person.inc');
/**
 * Include themeing functions
 *
 */
include_once('mdb_theme.inc');
/**
 * Include common functions
 *
 */
include_once('mdb_common.inc');


/**
 * Implementation of hook_help().
 *
 */
function moviedb_help($section = '') {

  $output = '';
  switch ($section) {
    case 'admin/help#moviedb':
      $output .= t('Moviedb is a module to help you manage a movie database.');
    case 'node/add#moviedb':
      $output = t('Add a movie.');
      break;
  }
  return $output;
}


/**
 * Implementation of hook_node_info().
 */
function moviedb_node_info() {
  return array(
    'mdb_movie'   => array('name' => t('Movie'), 'module' => 'mdb_movie'),
    'mdb_person'  => array('name' => t('Person'), 'module' => 'mdb_person'),
  );
}

/**
 * Implementation of hook_node_type().
 */
function moviedb_node_type($op, $info) {
  if (!empty($info->old_type) && $info->old_type != $info->type) {
    $update_count = node_type_update_nodes($info->old_type,
      $info->type);

    if ($update_count) {
      $substr_pre = 'Changed the content type of ';
      $substr_post = strtr(' from %old-type to %type.', array(
        '%old-type' => theme('placeholder', $info->old_type),
        '%type' => theme('placeholder', $info->type)));
      drupal_set_message(format_plural($update_count, $substr_pre
        .'@count post'. $substr_post, $substr_pre .'@count posts'.
        $substr_post));
    }
  }
}

/**
 * Implementation of hook_perm().
 */
function moviedb_perm() {
  return array('create movies', 'edit own movies', 'view movies', 'edit people' );
}

/**
 * Implementation of hook_menu().
 *
 */
function moviedb_menu($may_cache) {
  $items = array();

  if ($may_cache) {

  $items[] = array('path' => 'admin/settings/moviedb',
                     'title' => t('Movie Database'),
                     'description' => t('Set up the moviedb module'),
                     'callback' => 'drupal_get_form',
                     'callback arguments' => array('moviedb_admin_settings'),
                     'access' => user_access('administer site configuration'),
                     'type' => MENU_NORMAL_ITEM, );

  $items[] = array('path'     => 'moviedb/person/autocomplete',
                     'title'    => t('Person autocomplete'),
                     'callback' => 'mdb_person_autocomplete',
                     'access'   => user_access('view movies'),
                     'type' => MENU_CALLBACK,
  );
  }
  else {
  if ($css = variable_get('moviedb_css', drupal_get_path('module', 'moviedb') .'/moviedb.css')) {
 //add stylesheet
  drupal_add_css($css, 'all', TRUE);
    }
  }
  return $items;
}


/**
 * Settings Form
 *
 */
function moviedb_admin_settings() {
  if (!file_check_location(variable_get('moviedb_css', base_path() . drupal_get_path('module', 'moviedb') .'/moviedb.css'))) {
    $error['moviedb_css'] = theme('error', t('File does not exist, or is not readable.'));
  }
  if (module_exists('taxonomy')) {
    $vocs[0] = '<'. t('none') .'>';
    foreach (taxonomy_get_vocabularies() as $vid => $voc) {
      $vocs[$vid] = $voc->name;
    }
  }

  $form['moviedb_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('moviedb settings'),
  );

  $form['moviedb_settings']['moviedb_css'] = array(
    '#type' => 'textfield',
    '#title' => t('Style sheet'),
    '#default_value' => variable_get('moviedb_css', drupal_get_path('module', 'moviedb') .'/moviedb.css'),
    '#size' => 70,
    '#maxlength' => 255,
    '#description' => t("Specify the relative path to your moviedb style sheet. The style sheet specified here will be used to style your moviedb pages. If you prefer to style your moviedb pages in your theme, you can leave this field blank") . $error['moviedb_css'],
  );
  if ($voc) {
      $form['moviedb_settings']['moviedb_genre_vocab'] = array(
        '#type' => 'select',
        '#multiple' => FALSE,
        '#title' => t('Select the vocabulary to use for movie genres.'),
        '#default_value' => variable_get('moviedb_genre_vocab', 0),
        '#options' => $vocs,
        '#description' => t('Select the vocabulary you use to classify movie genres.'),
      );
  }

  return system_settings_form($form);
}