<?php
/**
 * @file
 * Tells the module what to do.
 */


/**
 * Manually create a person
 *
 */
function moviedb_person_add($name) {
  global $user;
  $actnode->uid = $user->uid;
  $actnode->name = $user->name;
  $actnode->type = 'mdb_person';
  $actnode->title = $name;
  $actnode->status = 1;

  //Make sure the data is ok and save him
  node_validate($actnode);
  if (!form_get_errors()) {
    $actnode = node_submit($actnode);
    node_save($actnode);
    return $actnode->nid;
  }
  else {
  return FALSE;
  }
}

/**
 * Check if a person with that name exists in the databasse
 *
 * @param string $name
 * @return nid
 */
function moviedb_person_exists($name) {

  $result = db_query_range("SELECT nid FROM {node} WHERE title = '%s'", $name, 0, 1);

  if (db_num_rows($result)) {
  $result = db_fetch_object($result);
  return $result->nid;
  }
  else {
  return FALSE;
  }

}

/**
 * Get the id of a person.
 *
 * @param string $name
 * @return nid
 */
function moviedb_person_get_id($name) {
  $id = moviedb_person_exists($name);
  if (!$id) {
  $id = moviedb_person_add($name);
  }

  return $id;
}

/**
 * Process people sent by the form
 *
 * @param int $mid the movie id
 * @param string $string the raw submitted people string
 * @param string $role the role
 */
function moviedb_people_process($mid, $string, $role) {

  if (!$table = moviedb_person_role_to_table($role)) {
  return FALSE;
  }

  //Put the people in a table
  $people = explode(', ', $string);

  //Loop through the people
  foreach ($people as $weight => $name) {

    //Trim the name
    $name = trim($name);

    //If the name is empty, go to next
    if (!$name) {
    continue;
    }

    //Get the id..
    $person_id = moviedb_person_get_id($name);

    //Something's wrong. don't add him
    if (!$person_id) {
    continue;
    }

    //Add the person to the movie
    db_query("INSERT INTO {$table} (mid, pid, weight) VALUES (%d, %d, %d)", $mid, $person_id, $weight);
  }
}

/**
 * Retrieve people from the database
 *
 * @param int $mid The movie id
 * @param string $role the role of the person
 * @return array
 */
function moviedb_people_load($mid, $role) {

  if (!$table = moviedb_person_role_to_table($role)) {
  return FALSE;
  }


  $people = db_query("SELECT p.title, p.nid, j.weight FROM {node} as p INNER JOIN {%s} as j ON p.nid = j.pid WHERE j.mid = %d ORDER BY j.weight ASC", $table, $mid);

  $return = FALSE;

  while ($person = db_fetch_object($people)) {
    $return['object'][] = $person;

    if ($return['form']) {
    $return['form'] .= ', '. $person->title;
    }
    else {
    $return['form'] .= $person->title;
    }
  }

  return $return;
}


/**
 * Get the movies a person participated in
 *
 * @param int $pid
 * The person's id
 * @param string $role
 * The role (director, actor...)
 *
 * @return array of movie objects
 */
function moviedb_person_movies($pid, $role) {

  if (!$table = moviedb_person_role_to_table($role)) {
  return FALSE;
  }

  $movies = db_query("SELECT n.title AS title, m.releaseyear AS releaseyear, n.nid AS nid FROM {node} as n INNER JOIN {%s} as j ON n.nid = j.mid INNER JOIN {moviedb} as m ON m.nid = n.nid WHERE j.pid = %d ORDER BY m.releaseyear DESC", $table, $pid);

  $return = FALSE;

  while ($movie = db_fetch_object($movies)) {
  $return[] = $movie;
  }

  return $return;

}

/**
 * Utility function to sort the filmography
 *
 * @param array $a
 * @param array $b
 * @return int
 */
function moviedb_person_usort($a, $b) {
  return count($a) - count($b);
}




/**
 * Get the table name from the person's role
 *
 * @param string $role
 * actor, director, writer, producer...
 *
 * @return string of the table name
 */
function moviedb_person_role_to_table($role) {
  switch ($role) {
    case 'actor':
    case 'actors':
    $table = 'moviedb_actors';
    break;

    case 'director':
    case 'directors':
    $table = 'moviedb_directors';
    break;

    case 'writer':
    case 'writers':
    $table = 'moviedb_writers';
    break;

    case 'producer':
    case 'producers':
    $table = 'moviedb_producers';
    break;

    //A wrong role.. exit now!
    default:
    $table = FALSE;
    break;
    }

    return $table;

}