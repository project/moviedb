<?php
/**
 * @file
 * The person settings
 */

/**
 * Implementation of hook_access().
 */
function mdb_person_access($op, $node) {
  global $user;

  if ($op == 'create') {
  return user_access('create movies');
  }

  if ($op == 'update' || $op == 'delete') {
  if (user_access('edit people')) {
  return TRUE;
    }
  }
}

/**
 * Implementation of hook_form().
 */
function mdb_person_form(&$node) {
  $form['title'] = array('#type' => 'textfield', '#title' => t('Name'), '#required' => TRUE, '#default_value' => $node->title, '#weight' => -5);
  $form['body_filter']['body'] = array('#type' => 'textarea', '#title' => t('Biography'), '#default_value' => $node->body, '#rows' => 6);
  $form['body_filter']['format'] = filter_form($node->format);
  return $form;
}

/**
 * Implementation of hook_delete().
 *
 */
function mdb_person_delete($node) {
  db_query("DELETE FROM {moviedb_actors} WHERE pid = %d", $node->nid);
  db_query("DELETE FROM {moviedb_directors} WHERE pid = %d", $node->nid);
  db_query("DELETE FROM {moviedb_writers} WHERE pid = %d", $node->nid);

}


/**
 * Implementation of hook_load().
 *
 */
function mdb_person_load($node) {

 //Get the person's filmography

  if ($actor = moviedb_person_movies($node->nid, 'actor')) {
  $movies->actor = $actor;
  }


  if ($director = moviedb_person_movies($node->nid, 'director')) {
  $movies->director = $director;
  }

  if ($writer = moviedb_person_movies($node->nid, 'writer')) {
  $movies->writer = $writer;
  }

  if ($producer = moviedb_person_movies($node->nid, 'producer')) {
  $movies->producer = $producer;
  }

  return $movies;

}

/**
 * Implementation of hook_view().
 *
 */
function mdb_person_view(&$node, $teaser = FALSE, $page = TRUE) {

  $node = node_prepare($node, $teaser);
  $node->content['data'] = array(
  '#value' => theme('mdb_person', $node),
  '#weight' => 0, );
  $node->content['body']['#value'] = '';

return $node;
}

function mdb_person_autocomplete($string) {

  // The user enters a comma-separated list of tags. We only autocomplete the last tag.
  // This regexp allows the following types of user input:
  // this, "somecmpany, llc", "and ""this"" w,o.rks", foo bar
  $regexp = '%(?:^|,\ *)("(?>[^"]*)(?>""[^"]* )*"|(?: [^",]*))%x';
  preg_match_all($regexp, $string, $matches);
  $array = $matches[1];

  // Fetch last tag
  $last_string = trim(array_pop($array));
  if ($last_string != '') {
    $result = db_query_range(db_rewrite_sql("SELECT title FROM {node} WHERE LOWER(title) LIKE LOWER('%s%%')"), $last_string, 0, 10);

    $prefix = count($array) ? implode(', ', $array) .', ' : '';

    $matches = array();
    while ($person = db_fetch_object($result)) {
      $n = $person->title;
      // Commas and quotes in terms are special cases, so encode 'em.
      if (preg_match('/,/', $person->title) || preg_match('/"/', $person->title)) {
        $n = '"'. preg_replace('/"/', '""', $person->title) .'"';
      }
      $matches[$prefix . $n] = check_plain($person->title);
    }
    print drupal_to_js($matches);
    exit();
  }
}